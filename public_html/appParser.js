// parentheses parse
function parseBracket(exp) {
    var z = 1,
        openBracket,
        closeBracket,
        inside,
        changed;

    do {
        openBracket = exp.lastIndexOf("(");

        if (openBracket < 0) {
            z = 0;
        } else {
            closeBracket = exp.indexOf(")", openBracket);
            inside = exp.slice(openBracket + 1, closeBracket);
            changed = parseAdd(parseDivided(parseMult(inside)));

            exp = exp.substr(0, openBracket) + changed.toString() + exp.substr(closeBracket + 1);
        }
    } while (z === 1)
    return exp;
}

// plus/minus parse
function parseAdd(line) {
    do {
        before = 1;
        if (line.charAt(0) === "-") {
            before = -1;
            line = line.slice(1);
        }
        kx = line.indexOf("+");
        ky = line.indexOf("-");
        if (kx === -1 && ky === -1) {
            line = (before * parseFloat(line)).toString();
            break
        } else {

            if ((kx > 0 && kx < ky) | (kx > 0 && ky === -1)) {
                lastz = kx;
                attr = 1;
            }
            if ((ky > 0 && ky < kx) | (ky > 0 && kx === -1)) {
                lastz = ky;
                attr = -1;
            }
            op1 = before * parseFloat(line.slice(0, lastz));

            arg = lastz + 1;
            do {
                if (line.charAt(arg) === "+" | line.charAt(arg) === "-") {
                    break
                }
                arg = arg + 1;
            } while (!(arg === line.length))
            op2 = attr * parseFloat(line.slice(lastz + 1, arg));

            res = op1 + op2;
            line = res.toString() + line.slice(arg);
        }
    } while (1 === 1)
    return line;
}

// mult parse
function parseMult(exp) {
    var k = 1,
        z,
        lastMult;

    do {
        lastMult = exp.lastIndexOf("*");
        if (lastMult === -1) {
            k = 0;
        } else {
            z = lastMult;
            do {
    // finding the first number
                beforez = z - 1;
                if (exp.charAt(beforez) === "*" | exp.charAt(beforez) === "/" | exp.charAt(beforez) === "-" | exp.charAt(beforez) === "+") {
                    z = -2;
                }
                z = z - 1;
            } while (z > -2)

            if (beforez === -2) {
                n1 = exp.slice(0, lastMult);
            } else {
                n1 = exp.slice(beforez + 1, lastMult);
            }

    // finding the second number
            z = lastMult;
            do {
                afterz = z + 1;
                if (exp.charAt(afterz) === "*" | exp.charAt(afterz) === "/" | exp.charAt(afterz) === "-" | exp.charAt(afterz) === "+") {
                    z = exp.length + 1;
                }
                z = z + 1;
            } while (z < exp.length + 1)

            if (afterz > exp.length) {
                n2 = exp.slice(lastMult + 1, exp.length);
            } else {
                n2 = exp.slice(lastMult + 1, afterz);
            }
            
    // result
            res = parseFloat(n1) * parseFloat(n2);
            exp = exp.substr(0, beforez + 1) + res.toString() + exp.substr(afterz);
        }
    } while (k === 1)
    return (exp);
}

// divided parse
function parseDivided(exp) {
    var k = 1,
        z,
        lastDiv;

    do {
        lastDiv = exp.lastIndexOf("/");
        if (lastDiv === -1) {
            k = 0;
        } else {
    // finding the first number
            z = lastDiv;
            do {
                beforez = z - 1;
                if (exp.charAt(beforez) === "*" | exp.charAt(beforez) === "/" | exp.charAt(beforez) === "-" | exp.charAt(beforez) === "+") {
                    z = -2;
                }
                z = z - 1;
            } while (z > -2)

            if (beforez === -2) {
                n1 = exp.slice(0, lastDiv);
            } else {
                n1 = exp.slice(beforez + 1, lastDiv);
            }

    // finding the second number
            z = lastDiv;
            do {
                afterz = z + 1;
                if (exp.charAt(afterz) === "*" | exp.charAt(afterz) === "/" | exp.charAt(afterz) === "-" | exp.charAt(afterz) === "+") {
                    z = exp.length + 1;
                }
                z = z + 1;
            } while (z < exp.length + 1)

            if (afterz > exp.length) {
                n2 = exp.slice(lastDiv + 1, exp.length);
            } else {
                n2 = exp.slice(lastDiv + 1, afterz);
            }
            
    // result
            res = parseFloat(n1) / parseFloat(n2);
            exp = exp.substr(0, beforez + 1) + res.toString() + exp.substr(afterz);
        }
    } while (k === 1)

    return (exp);
}