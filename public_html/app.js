(function () {
    window.addEventListener('load', init);

    function init() {
        var keyPad = document.getElementById('keyPad'),
            dispForm = document.getElementById('form'),
            display = document.getElementById('disp'),
            lngChek = true,
            mouseDown = false;

        keyPad.innerHTML = '<div class="keys">7</div>'
            + '<div class="keys">8</div>'
            + '<div class="keys">9</div>'
            + '<div class="keys key-gray">+</div>'
            + '<div class="keys key-red">&#8592</div>'
            + '<div class="keys">4</div>'
            + '<div class="keys">5</div>'
            + '<div class="keys">6</div>'
            + '<div class="keys key-gray">-</div>'
            + '<div class="keys key-gray">(</div>'
            + '<div class="keys">1</div>'
            + '<div class="keys">2</div>'
            + '<div class="keys">3</div>'
            + '<div class="keys key-gray">*</div>'
            + '<div class="keys key-gray">)</div>'
            + '<div class="keys">00</div>'
            + '<div class="keys">0</div>'
            + '<div class="keys">.</div>'
            + '<div class="keys key-gray">/</div>'
            + '<div class="keys key-blue">=</div>';


        keyPad.addEventListener('mousedown', function (e) {
            mouseDown = true;

            if (e.target.parentElement.id === 'keyPad') {
                dispAction(e.target.innerHTML);
            }
        });

        keyPad.addEventListener('mouseup', function () {
            mouseDown = false;
        });

        display.addEventListener('input', function (e) {
            console.log(e);
            if ('0123456789-+./*()'.indexOf(e.data) === -1) {
                display.value = display.value.replace(e.data, '');
            } else if (display.value.length > 1 && display.value[0] === '0' && display.value[1] !== '.') {
                display.value = display.value.slice(1);
            }
            lengthChek();
        });


        function lengthChek() {
            if (display.value.length >= 1 && display.value.length < 10) {
                display.style.fontSize = '3.5em';
                lngChek = true;
            }
            if (display.value.length >= 10 && display.value.length < 20) {
                display.style.fontSize = '2.5em';
                lngChek = true;
            }
            if (display.value.length >= 20 && display.value.length < 30) {
                display.style.fontSize = '1.75em';
                lngChek = true;
            }
            if (display.value.length >= 30) {
                lngChek = false;
            }
        }

        function dispAction(val) {
            lengthChek();

            if (lngChek && val !== '←' && val !== '=') {
                if (display.value === '0' && val !== '.' || display.value === '00' && val !== '.') {
                    display.value = val;
                } else if (display.value === '00') {
                    display.value = '0' + val;
                } else {
                    display.value += val;
                }
            }
            if (val === '←') {
                if (display.value.length === 1 || display.value.length === 0) {
                    display.value = display.value.slice(0, -1);
                    display.value = '0';
                } else if (display.value === 'incorrect expression') {
                    display.value = 0;
                } else {
                    display.value = display.value.slice(0, -1);
                }
            }
            if (val === '=') {
                dispForm.innerHTML = display.value + '=';

                equalResult();
            }
        }

        function equalResult() {
            var valid = isValid(display.value);

            if (valid === 1) {
                display.style.fontSize = '2em';
                display.value = 'incorrect expression';
            } else if (valid === 0) {
                display.value = parseAdd(parseDivided(parseMult(parseBracket(display.value))));
            }
        }


        function isValid(exp) {
            var check = 0,
                fault = 0;

            // bracked verify
            for (i = 0; i < exp.length; i++) {
                if (exp.charAt(i) === "(") {
                    check += 1;
                }
                if (exp.charAt(i) === ")") {
                    check -= 1;
                }
                if (check < 0) {
                    fault = 1;
                }
            }
            if (!(check === 0)) {
                fault = 1;
            }

            // operators duplicate verify
            for (i = 0; i < exp.length; i++) {
                if ((exp.charAt(i) === "(" | exp.charAt(i) === "+" | exp.charAt(i) === "-" | exp.charAt(i) === "*" | exp.charAt(i) === "/") && (exp.charAt(i + 1) === ")" | exp.charAt(i + 1) === "+" | exp.charAt(i + 1) === "-" | exp.charAt(i + 1) === "*" | exp.charAt(i + 1) === "/")) {
                    fault = 1;
                }
            }

            // valid characters verify
            var dig = /\d|\./;
            for (i = 0; i < exp.length; i++) {
                if ((exp.charAt(i).search(dig) === -1) && (!(exp.charAt(i) === "+" | exp.charAt(i) === "-" | exp.charAt(i) === "*" | exp.charAt(i) === "/" | exp.charAt(i) === "(" | exp.charAt(i) === ")"))) {
                    fault = 1;
                }
            }
            if ((exp.charAt(exp.length - 1).search(dig) === -1) && (!(exp.charAt(exp.length - 1) === ")"))) {
                fault = 1;
            }

            return fault;
        }

    }
})();